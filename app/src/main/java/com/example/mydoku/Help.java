package com.example.mydoku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Help extends AppCompatActivity {

    Toolbar bantuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        bantuan = findViewById(R.id.help);
        bantuan.setTitle("Bantuan");
        setSupportActionBar(bantuan);

        BottomNavigationView navigasi = findViewById(R.id.menu);
        navigasi.setSelectedItemId(R.id.profil);

        navigasi.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.trans:
                        startActivity(new Intent(getApplicationContext()
                                , Transaksi.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.utang:
                        startActivity(new Intent(getApplicationContext(), Hutang.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.profil:
                        startActivity(new Intent(getApplicationContext(), Profile.class));
                        overridePendingTransition(0, 0);
                        return true;

                }

                return false;

            }
        });

    }

}