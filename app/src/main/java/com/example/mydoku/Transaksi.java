package com.example.mydoku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

public class Transaksi extends AppCompatActivity {

    TextView tvIncome, tvExpanses, tvBalance, tvEmail;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    Cursor cursor;
    private SessionManager sessionManagement;
    RecyclerView rv;
    private ArrayList<String> dataId,dataTanggal, dataKeterangan, dataJumlah;
    DatabaseHelper dbcenter;
    String id="";
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_transaksi );

        setupToolbar();

        BottomNavigationView navigasi = findViewById( R.id.menu );
        navigasi.setSelectedItemId( R.id.trans );

        navigasi.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.trans:
                        return true;

                    case R.id.utang:
                        startActivity( new Intent( getApplicationContext(), Hutang.class ) );
                        overridePendingTransition( 0, 0 );
                        return true;

                    case R.id.profil:
                        startActivity( new Intent( getApplicationContext(), Profile.class ) );
                        overridePendingTransition( 0, 0 );
                        return true;

                }

                return false;

            }
        } );

        dbcenter = new DatabaseHelper( this );
        sessionManagement = new SessionManager( Transaksi.this );
        tvIncome = findViewById( R.id.income );
        tvExpanses = findViewById( R.id.expenses );
        tvBalance = findViewById( R.id.balance );
        tvEmail = findViewById( R.id.txtEmail);

        dataId = new ArrayList<>();
        dataTanggal = new ArrayList<>();
        dataKeterangan=new ArrayList<>();
        dataJumlah = new ArrayList<>();


        if(sessionManagement.isLoggedIn()) {
            final HashMap<String, String> user = sessionManagement.getUserInformation();
            SQLiteDatabase db = dbcenter.getReadableDatabase();
            int i = 0;
            int e = 0;
            //mengambil jumlah income user dari sqlite
            cursor = db.rawQuery("SELECT SUM(jumlah) FROM transaksi WHERE id_user = '" + user.get(sessionManagement.KEY_ID_USER) + "' and jenis = 'Pemasukan'", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) //jika hasil query tidak kosong
            {
                cursor.moveToPosition(0);
                if (cursor.isNull(0)) {
                    tvIncome.setText("0"); //jika jumlah nya null isi text view dengan angka 0
                    i = 0;
                } else { //jika tidak null isi sesuai databasw
                    tvIncome.setText(cursor.getString(0).toString());
                    i = Integer.parseInt(cursor.getString(0).toString());
                }

            }
            //mengambil jumlah expenses user dari sqlite
            cursor = db.rawQuery("SELECT SUM(jumlah) FROM transaksi WHERE id_user = '" + user.get(sessionManagement.KEY_ID_USER) + "' and jenis = 'Pengeluaran'", null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) //jika hasil query tidak kosong
            {
                cursor.moveToPosition(0);
                if (cursor.isNull(0)) { //jika jumlah nya null isi text view dengan angka 0
                    tvExpanses.setText("0");
                    e = 0;
                } else{
                    tvExpanses.setText(cursor.getString(0).toString());
                    e = Integer.parseInt(cursor.getString(0).toString());
                }
            }
            int total = i-e; //total diperoleh dari income - expenses
            tvBalance.setText(Integer.toString(total));
            id=user.get(sessionManagement.KEY_ID_USER);

        }



        initDataset();
        rv = (RecyclerView) findViewById(R.id.rv_main);
        rv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);
        adapter = new MyAdapter(dataId,dataTanggal,dataKeterangan,dataJumlah);
        rv.setAdapter(adapter);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent m = new Intent(Transaksi.this, TambahCatatanTransaksi.class);
                startActivity(m);
                finish();
            }
        });
    }

    private void initDataset() {
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        //mengambil data dari database untuk isi dari recycler view
        cursor = db.rawQuery("SELECT * FROM transaksi where id_user ="+ id,null);
        cursor.moveToFirst();
        for (int cc=0; cc < cursor.getCount(); cc++){
            cursor.moveToPosition(cc);
            //masukkan data kedalam array
            dataId.add(cursor.getString(0).toString());
            dataTanggal.add(cursor.getString(1).toString());
            dataKeterangan.add(cursor.getString(6).toString());
            dataJumlah.add(cursor.getString(5).toString());
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Transaksi");
        setSupportActionBar(toolbar);
    }

}