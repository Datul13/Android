package com.example.mydoku;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    DatabaseHelper db;
    Button login;
    FloatingActionButton register;
    EditText username, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHelper(this);

        username = (EditText)findViewById(R.id.R_username);
        email = (EditText)findViewById(R.id.R_email);
        password = (EditText)findViewById(R.id.R_pass);
        login = (Button)findViewById(R.id.login);
        register = (FloatingActionButton) findViewById(R.id.regis);

        //register
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strUsername = username.getText().toString();
                String strEmail = email.getText().toString();
                String strPassword = password.getText().toString();
                if(strEmail.equals("") || strEmail.trim().isEmpty() || strPassword.equals("") || strPassword.trim().isEmpty() ||
                        strUsername.equals("") || strUsername.trim().isEmpty())//cek kondisi agar kolom tidak kosong
                {
                    Toast.makeText(Register.this,"Semua kolom harus diisi",Toast.LENGTH_LONG).show();
                }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()){//cek format email
                    Toast.makeText(Register.this,"Masukkan format email yang benar",Toast.LENGTH_LONG).show();

                }else{//jika semua kondisi terpenuhi maka berhasil

                    db.addUser(strEmail, strUsername, strPassword); //menjalankan method untuk menambah data user
                    Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
                    Intent mIntent = new Intent(getApplicationContext(),login.class);
                    startActivity(mIntent);}//berpindah ke halaman login
            }

        });

        //login
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(Register.this, login.class);
                startActivity(loginIntent);
                finish();
            }
        });


    }


}