package com.example.mydoku;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;

public class Profile extends AppCompatActivity {

    protected Cursor cursor;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    SessionManager session;
    String name, email;
    Button help, logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );

        setupToolbar();

        dbHelper = new DatabaseHelper(this);

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();
        email = user.get(SessionManager.KEY_EMAIL);
        name = user.get(SessionManager.KEY_USERNAME);
        db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM user WHERE username = '" + email + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            name = cursor.getString(2);
        }

        TextView lblName = findViewById(R.id.lblName);
        TextView lblEmail = findViewById(R.id.lblEmail);

        lblName.setText(name);
        lblEmail.setText(email);

        help = (Button)findViewById(R.id.btnhelp);
        help.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Edit = new Intent(Profile.this, Help.class);
                startActivity(Edit);
                finish();
            }
        } );

        logout = (Button)findViewById(R.id.btnlogout);
        logout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(Profile.this)
                        .setTitle("Anda yakin ingin keluar ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                session.logoutUser();
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .create();
                dialog.show();
            }
        } );

        BottomNavigationView navigasi = findViewById(R.id.menu);
        navigasi.setSelectedItemId(R.id.profil);

        navigasi.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.trans:
                        startActivity(new Intent(getApplicationContext()
                                , Transaksi.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.utang:
                        startActivity(new Intent(getApplicationContext(), Hutang.class));
                        overridePendingTransition(0, 0);
                        return true;

                    case R.id.profil:
                        return true;

                }

                return false;

            }
        } );

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.tbProfil);
        toolbar.setTitle("Profil Pengguna");
        setSupportActionBar(toolbar);
    }
}