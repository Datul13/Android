package com.example.mydoku;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {

    DatabaseHelper db;
    FloatingActionButton login;
    Button register;
    EditText email, password;
    SessionManager sessionManager;
    protected Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(login.this);

        db = new DatabaseHelper(this);

        email = (EditText)findViewById(R.id.email1);
        password = (EditText)findViewById(R.id.password);
        login = (FloatingActionButton)findViewById(R.id.login);
        register = (Button)findViewById(R.id.regis);

        if(sessionManager.isLoggedIn()){ //jika sudah login maka menjalankan function goToActivity
            goToActivity();;
        }

        //login
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strEmail = email.getText().toString();
                String strPassword = password.getText().toString();
                if(strEmail.equals("") || strEmail.trim().isEmpty() || strPassword.equals("") || strPassword.trim().isEmpty()  ) {
                    //memastikan agar email tidak kosong
                    Toast.makeText(login.this,"Username Password harus diisi",Toast.LENGTH_LONG).show();
                }else if(!(strEmail.contains("@"))){
                    //memastikan email sesuai format
                    Toast.makeText(login.this,"Masukkan format email yang benar",Toast.LENGTH_LONG).show();
                }
                else
                {
                    SQLiteDatabase dbs = db.getReadableDatabase();
                    cursor = dbs.rawQuery("SELECT * FROM user WHERE email = '" + strEmail + "' AND password = '"+ strPassword+ "'",null);
                    cursor.moveToFirst();
                    if(cursor.getCount()>0){
                        //membuat session
                        sessionManager.createLoginSession(cursor.getString(0).toString(),cursor.getString(1).toString(),cursor.getString(2).toString(), strPassword);
                        goToActivity();
                    }else {
                        //membuat peringatan jika data user tidak ada di database
                        Toast.makeText(login.this, "Username Password harus ada di database", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        //register
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(login.this, Register.class);
                startActivity(registerIntent);
                finish();
            }
        });

    }

    private void goToActivity(){
        Intent mIntent = new Intent(getApplicationContext(),Transaksi.class);
        startActivity(mIntent);
    }

}